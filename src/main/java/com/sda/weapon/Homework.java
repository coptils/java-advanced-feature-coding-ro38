package com.sda.weapon;

/**
 * Create a class imitating a weapon magazine.
 * The class should be able to define the size of the magazine using the constructor.
 * <p>
 * Implement the methods:
 * loadBullet(String bullet) → adding a cartridge to the magazine, does not allow loading more cartridges
 * than the capacity of the magazine
 * <p>
 * isLoaded() → returns information about whether the weapon is loaded (at least one cartridge) or not
 * <p>
 * shot() → each call shots one bullet (prints string value in console) - the last loaded cartridge - and
 * prepares the next one, loaded before the last one, if there are no more cartridges, it prints "empty
 * magazine" in the console
 */
public class Homework {
    public static void main(String[] args) {
        Magazine m1 = new Magazine(7);
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("1");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("2");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("3");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("4");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("5");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("6");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("7");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("8");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInMagazine("9");
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();

        // We need to load the first bullet into the chamber, before shooting or
        // after the last bullet was shot
        m1.loadBulletInChamber();

        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();


        m1.shoot();
        m1.shoot();

        m1.loadBulletInMagazine("7");
        m1.shoot();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.loadBulletInChamber();
        System.out.print("Magazine status: ");
        m1.displayIsMagazineLoaded();
        m1.shoot();

        System.out.println("Adding a new set of bullets: ");
        m1.loadBulletInMagazine("1");
        m1.loadBulletInMagazine("2");
        m1.loadBulletInMagazine("3");
        m1.loadBulletInMagazine("4");
        m1.loadBulletInMagazine("5");
        m1.loadBulletInMagazine("6");
        m1.loadBulletInMagazine("7");
        m1.displayIsMagazineLoaded();

        m1.sprayAndPray();
        m1.loadBulletInChamber();
        m1.sprayAndPray();

    }
}
