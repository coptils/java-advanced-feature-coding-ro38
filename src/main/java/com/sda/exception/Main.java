package com.sda.exception;

public class Main {
    public static void main(String[] args) {

//        throwUnCheckedException();

        try {
            throwUpperCheckedException();
        } catch (CheckedException e) {
            System.out.println("We have catch an checked exception in main method");
        }

//        throwUnCheckedException();
    }

    // checked exception
    private static void handleCheckedException() {
        try {
            throwCheckedException();
        } catch (CheckedException ex) {
            System.out.println("We have catch a checked exception");
        }
    }

    private static void throwUpperCheckedException() throws CheckedException {
        throwCheckedException();
    }

    private static void throwCheckedException() throws CheckedException {
        System.out.println("Exception...");
        throw new CheckedException("checked");
    }

    // unchecked exceptions

    private static void noHandlingMechanismNeededForUncheckedException() {
        throwUnCheckedException();
        throwUnCheckedExceptionV2();
    }

    private static void throwUnCheckedExceptionV2() {
        System.out.println("Exception...");
        throw new UncheckedException("unchecked");
    }
    private static void throwUnCheckedException() throws UncheckedException {
        System.out.println("Exception...");
        throw new UncheckedException("unchecked");
    }

}
