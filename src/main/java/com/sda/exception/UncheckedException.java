package com.sda.exception;

public class UncheckedException extends RuntimeException {
    public UncheckedException(String message) {
        super(message);
    }
}
