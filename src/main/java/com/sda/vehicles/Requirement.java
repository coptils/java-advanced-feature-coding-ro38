package com.sda.vehicles;

import com.sda.autoshop.model.Car;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * ## Vehicle IO application requirements
 * <p>
 * Build an OOP hierarchy while considering the following types of objects:
 * <p>
 * Cars
 * Brand
 * Model
 * Price
 * TopSpeed
 * Transmission(Manual/Automatic)
 * Shape(Coupe/Sedan/Wagon)
 * <p>
 * Motorcycles
 * Brand
 * Model
 * Price
 * TopSpeed
 * Shape(Chopper/Cruiser/Enduro)
 * <p>
 * Tractors
 * Brand
 * Model
 * Price
 * MaxPulledWeight
 * <p>
 * <p>
 * * Read vehicles.txt (found under resources folder) and create objects of the proper type
 * * Count the number of cars, motorcycles, tractors
 * * Count how many vehicles of each brand are there
 * * Sort the cars by price
 * * Sort the choppers by top speed
 * * Display each category of vehicles in separate files
 */
public class Requirement {
    public static void main(String[] args) {
        List<String> lines;
        try{
            lines = Files.readAllLines(Paths.get("src/main/resources/vehicles.txt"));
        }catch(IOException ioException){
            lines = new ArrayList<>();
            System.out.println("Error!");
        }
        for (String line:lines) {
            System.out.println(line);
            Vehicles v = covertLineToVehicles(line);

        }

    }
    private static Vehicles covertLineToVehicles(String line){
        String[] splitLine = line.split(",");

        Vehicles vehicles = new Vehicles();
        switch (splitLine[0]){
            case "Car":
                vehicles = convertLineToCar(splitLine);
                break;
            case "Motorcycles":
                vehicles = convertLineToMotorcycles(splitLine);
                break;
        }
        return vehicles;
    }
    private static Cars convertLineToCar(String[] arrayCar){

        Cars car1 = new Cars();
        //set car fields
        return car1;
    }
    private static Motorcycles convertLineToMotorcycles(String[] arrayMotorcycles) {

        Motorcycles car1 = new Motorcycles();
        //set car fields
        return car1;
    }
}
