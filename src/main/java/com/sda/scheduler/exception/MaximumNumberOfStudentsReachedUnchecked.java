package com.sda.scheduler.exception;

public class MaximumNumberOfStudentsReachedUnchecked extends RuntimeException {

    public MaximumNumberOfStudentsReachedUnchecked(String message) {
        super(message);
    }
}
