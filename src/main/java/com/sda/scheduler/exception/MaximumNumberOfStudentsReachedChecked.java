package com.sda.scheduler.exception;

public class MaximumNumberOfStudentsReachedChecked extends Exception {

    public MaximumNumberOfStudentsReachedChecked(String message) {
        super(message);
    }
}
