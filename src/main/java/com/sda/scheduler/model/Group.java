package com.sda.scheduler.model;

import com.sda.scheduler.exception.MaximumNumberOfStudentsReachedChecked;
import com.sda.scheduler.exception.MaximumNumberOfStudentsReachedUnchecked;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Group {

    private String name;
    private Trainer trainer;
    private Set<Student> students =new HashSet<>();

    public String getName() {
        return name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public Set<Student> getStudents() {
        Set<Student> copy =new HashSet<>();
        copy.addAll(this.students);
        return copy;
    }
    public Set<Student> getStudentsV2() {
        return Collections.unmodifiableSet(this.students);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public void addStudent(Student student) throws MaximumNumberOfStudentsReachedChecked, Exception {
        if(this.students.size()>=5){
            throw new MaximumNumberOfStudentsReachedChecked("Group limit reached");
        }else{
            students.add(student);
        }
    }

    public void removeStudentYoungerThan(int age){
        this.students.removeIf(student ->student.getAge()<age);
    }


    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", trainer=" + trainer +
                ", students=" + students +
                '}';
    }
}
