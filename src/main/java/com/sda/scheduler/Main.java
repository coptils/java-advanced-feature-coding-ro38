package com.sda.scheduler;

import com.sda.scheduler.exception.MaximumNumberOfStudentsReachedUnchecked;
import com.sda.scheduler.model.Group;
import com.sda.scheduler.model.Person;
import com.sda.scheduler.model.Student;
import com.sda.scheduler.model.Trainer;
import com.sda.scheduler.repository.Storage;

import java.time.LocalDate;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static com.sda.scheduler.repository.Storage.getGroups;
import static com.sda.scheduler.repository.Storage.getStudents;

/**
 * SDA Scheduler application requirements
 * <p>
 * Create a class hierarchy
 * <p>
 * Person.java - firstname
 * - lastname
 * - dateOfBirth
 * Trainer.java (extends Person) - isAuthorized (boolean)
 * Student.java (extends Person) - hasPreviousJavaKnowledge (boolean)
 * <p>
 * Create a Group class which has
 * <p>
 * name (Java2Gdansk, Tester3Bucharest, etc)
 * one trainer
 * a list of students
 * <p>
 * * Manually initialize 15 students; 4 groups and 3 trainers;
 * * Store all students in a list; all groups in a list; all trainers in a list;
 * * Asign a trainer to each group
 * * Asign 3-4 students to each group
 * * Ensure the fact that a group will only have distinct students (How would you do that?)
 * * Ensure the fact that a group will only have a maximum of 5 students;
 * When you try to add a 6th one throw an MaximumNumberOfStudentsReached exception
 * * 1. Display all students sorted alphabetically by lastName
 * * 2. Display the group with the maximum number of students
 * * 3. Display all students grouped by trainer that teaches to them
 * (eg. Trainer1 - stud1, stud3, stud4; Trainer2 - stud2, stud 10) - regardless of the group they're part of
 * (If you were to store this information in a data structure what would you use?)
 * * 4. Display all students with previous java knowledge
 * * Homework:
 * * 5. Display the group with the highest number of students with no previous java knowledge
 * * 6. Display all students younger than 25, from all groups; Period - for finding the age of the students  (ex. Period.between(student.getDateOfBirth(), LocalDate.now()).getYears(); )
 * * 7. Remove all the students younger than 20 from all groups; Hint: add a new method in Group class that do the delete of the student; removeIf(condition)
 */

public class Main {
    public static void main(String[] args) {

        Group g1 = new Group();
        g1.setName("JavaRo38");
        g1.setTrainer(new Trainer("Alex", "Popescu", LocalDate.of(2000, 12, 15), true));
        try {
            g1.addStudent(new Student("Alex", "Popescu", LocalDate.of(1998, 05, 14), false));
            g1.addStudent(new Student("Alex", "Popescu", LocalDate.of(1997, 05, 14), false));
            g1.addStudent(new Student("Alex", "Popescu", LocalDate.of(1998, 05, 15), false));
            g1.addStudent(new Student("Paul", "Popescu", LocalDate.of(1998, 05, 14), false));
            g1.addStudent(new Student("Alex", "Popescu", LocalDate.of(1998, 05, 14), true));
            g1.addStudent(new Student("Ion", "Popescu", LocalDate.of(1998, 05, 14), true));
            g1.addStudent(new Student("Ana", "Popescu", LocalDate.of(1998, 05, 14), false));
        } catch (MaximumNumberOfStudentsReachedUnchecked ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("General exception. Reason:  " + ex.getMessage());
        }
        System.out.println(g1);

        Set<Student> originalStudents = g1.getStudentsV2();
        // originalStudents.add(new Student("Alex", "Constantinescu", LocalDate.of(1998, 05, 14), false));


        System.out.println(g1);

        // Camera 1
        sortStudentsByLastName();
        displayGroupWithMaxNumberOfStudentsWithoutJavaKnowledge();

        // Camera 2
        getGroupsWithMostStudents();
        getStudentsWithPreviousJavaKnowledge();
        getGroupWithMostStudentsWithNoJavaKnowledge();
        sortStudentsByTrainer();

        System.out.println("===================");
        getStudentUnderCertainAge(25);

        removeAllStudentsUnderACertainAge(20);

    }

    public static void removeAllStudentsUnderACertainAge(int age) {
        List<Group> groups = Storage.getGroups();
        for (int i = 0; i < groups.size(); i++) {
            groups.get(i).removeStudentYoungerThan(age);
        }
        for (Group group : groups) { //stanga - tipul elementelor din lista; dreapta- numele listei
            System.out.println(group);
        }
        System.out.println(groups);
    }

    public static void sortStudentsByLastName() {
        List<Student> sortedStudentByLastName = Storage.getStudents().stream().sorted(Comparator.comparing(Person::getLastName)).collect(Collectors.toList());
        System.out.println(sortedStudentByLastName);
    }

    public static void displayGroupWithMaxNumberOfStudentsWithoutJavaKnowledge() {
        List<Group> groups = Storage.getGroups();

        Group max = groups.get(0);
        for (Group group : groups) {
            if (getNumberOfStudentsWithoutJavaKnowledge(group.getStudents()) > getNumberOfStudentsWithoutJavaKnowledge(max.getStudents())) {
                max = group;
            }
        }

        System.out.println(max);
    }

    public static int getNumberOfStudentsWithoutJavaKnowledge(Set<Student> students) {
        int numberOfStudents = 0;

        for (Student student : students) {
            if (!student.isHasPreviousJavaKnowledge()) {
                numberOfStudents++;
            }
        }
        return numberOfStudents;
    }

    // * * 2. Display the group with the most number of students
    public static void getGroupsWithMostStudents() {
        Group groupWithMostStudents = getGroups().stream().max(new Comparator<Group>() {
            @Override
            public int compare(Group o1, Group o2) {
                int nrOfStudentsForGroup1 = o1.getStudents().size();
                int nrOfStudentsForGroup2 = o2.getStudents().size();
                return Integer.compare(nrOfStudentsForGroup1, nrOfStudentsForGroup2);
            }
        }).orElse(null);
        System.out.println(groupWithMostStudents.getName());
    }

    //* * 4. Display all students with previous java knowledge
    public static void getStudentsWithPreviousJavaKnowledge() {
        List<Student> studentsWithPreviousJavaKnowledge = getStudents();
        studentsWithPreviousJavaKnowledge.stream().filter(student -> student.isHasPreviousJavaKnowledge() == true).forEach(student -> System.out.println(student.getFirstName()));
    }

    // * * 5. Display the group with the highest number of students with no previous java knowledge
    public static void getGroupWithMostStudentsWithNoJavaKnowledge() {
        Group groupWithMostStudentsWithNoJavaKnowledge = getGroups().stream().max(new Comparator<Group>() {
            @Override
            public int compare(Group o1, Group o2) {
                int nrOfStudentsForGroup1 = o1.getStudents().stream().filter(student -> !student.isHasPreviousJavaKnowledge()).toList().size();
                int nrOfStudentsForGroup2 = o2.getStudents().stream().filter(student -> !student.isHasPreviousJavaKnowledge()).toList().size();
                return Integer.compare(nrOfStudentsForGroup1, nrOfStudentsForGroup2);
            }
        }).orElse(null);
        System.out.println(groupWithMostStudentsWithNoJavaKnowledge.getName());

    }

    //* * 3. Display all students grouped by trainer that teaches to them
    //* (eg. Trainer1 - stud1, stud3, stud4; Trainer2 - stud2, stud 10) - regardless of the group they're part of
    //* (If you were to store this information in a data structure what would you use?)

    public static void sortStudentsByTrainer() {
        Map<Trainer, Set<Student>> trainersWithStudents = new HashMap<>();
        List<Group> groups = Storage.getGroups();
        for (Group g : groups) {
            if (!trainersWithStudents.containsKey(g.getTrainer())) {
                trainersWithStudents.put(g.getTrainer(), g.getStudents());
            } else {
                Set<Student> existingStudents = trainersWithStudents.get(g.getTrainer());
                existingStudents.addAll(g.getStudents());
                trainersWithStudents.put(g.getTrainer(), existingStudents);
            }
        }
        displayTrainersWithTheirStudents(trainersWithStudents);
    }

    private static void displayTrainersWithTheirStudents(Map<Trainer, Set<Student>> trainersWithStudents) {
        trainersWithStudents.forEach((trainer, students) -> {
            displayTrinerWithStudents(trainer, students);
        });
    }

    private static void displayTrainersWithTheirStudentsV2(Map<Trainer, Set<Student>> trainersWithStudents) {
        trainersWithStudents.forEach(new BiConsumer<Trainer, Set<Student>>() {
            @Override
            public void accept(Trainer trainer, Set<Student> students) {
                displayTrinerWithStudents(trainer, students);
            }
        });
    }

    private static void displayTrainersWithTheirStudentsV3(Map<Trainer, Set<Student>> trainersWithStudents) {
        trainersWithStudents.forEach((Trainer trainer, Set<Student> students) -> {
            displayTrinerWithStudents(trainer, students);
        });
    }

    private static void displayTrainersWithTheirStudentsV4(Map<Trainer, Set<Student>> trainersWithStudents) {
        trainersWithStudents.forEach((trainer, students) -> {
            displayTrinerWithStudents(trainer, students);
        });
    }

    private static void displayTrainersWithTheirStudentsV5(Map<Trainer, Set<Student>> trainersWithStudents) {
        trainersWithStudents.forEach((trainer, students) -> displayTrinerWithStudents(trainer, students));
    }

    private static void displayTrinerWithStudents(Trainer trainer, Set<Student> students) {
        System.out.println("For Trainer: " + trainer);
        System.out.println("Trainer has: " + students.size());
        System.out.println("The list of students is: " + students);
    }


    //  6. Display all students younger than 25, from all groups;
    // Period - for finding the age of the students
    // (ex. Period.between(student.getDateOfBirth(), LocalDate.now()).getYears(); )
    public static void getStudentUnderCertainAge(int age) {
        getStudents().stream().filter(student -> {
            if (student.getAge() < age) {
                return true;
            }
            return false;

        }).forEach(student -> System.out.println(student.getFirstName()));
    }

    // varianta 2
    public static void getStudentUnderCertainAgeV2(int age) {
        getStudents().stream()
                .filter(student -> student.getAge() < age)
                .forEach(student -> System.out.println(student.getFirstName()));
    }

    // varianta 3
    public static void getStudentUnderCertainAgeV3(int age) {
        getStudents().stream()
//                .filter(new Predicate<Student>() {
//                    @Override
//                    public boolean test(Student student) {
//                        return student.getAge() < age;
//                    }
//                })
//                .filter((Student student) -> {
//                    return student.getAge() < age;
//                })
//                .filter((Student student) -> student.getAge() < age)
                .filter(student -> student.getAge() < age)
                .forEach(student -> System.out.println(student.getFirstName()));
    }
}