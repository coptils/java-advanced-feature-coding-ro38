package com.sda.agenda;

import java.util.HashSet;
import java.util.Set;

public class Agenda {
    private static final int MAX_CAP = 100;
    private Set<Contact> contacts;

    Agenda() {
        this.contacts = new HashSet<>();
    }

    public void addContacts(Set<Contact> contacts) {
        if (this.contacts.size() < MAX_CAP) {
            this.contacts.addAll(contacts);
        } else {
            System.out.println("too many contacts");
        }

    }

    public void addContacts1(Set<Contact> contacts) {
        for (Contact temp : contacts) {
            if (this.contacts.size() < MAX_CAP) {
                this.contacts.add(temp);
            }
        }
    }

    public void addContact(Contact contact) {
        if (this.contacts.size() < MAX_CAP) {
            this.contacts.add(contact);
        } else {
            System.out.println("Memory full");
        }
    }

    public boolean containsContact(Contact contact) {
        boolean result = this.contacts.stream().anyMatch(c -> c.equals(contact));
        return result;
    }

    @Override
    public String toString() {
        return "Agenda{" +
                "contacts=" + contacts +
                '}';
    }
}