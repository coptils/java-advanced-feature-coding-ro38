package com.sda.agenda;

import java.util.Set;

/**
 * Agenda telefonica
 * Creati o agenda pentru numere de telefon de capacitate 100;
 * Putem sa adaugam un numar in agenda doar daca nu s-a atins capacitatea maxima.
 * Putem sa cautam in agenda dupa un numar de telefon sa vedem daca acesta exista sau nu.
 * Putem sa afisam toate numerele de telefon existente in agenda.
 */

public class Main {
    public static void main(String[] args) {
        Contact c1 = new Contact("Marius", "0722345987");
        Contact c2 = new Contact("Cleo", "0722345999");
        Contact c3 = new Contact("Stefan", "0722345227");
        Contact c4 = new Contact("Alex", "0722345227");

        Agenda a1 = new Agenda();
        //a1.addContact(c1);
        //a1.addContact(c2);
        //a1.addContact(c3);
        a1.addContacts(Set.of(c1,c2,c3));

        boolean result = a1.containsContact(c1);
        System.out.println("c1 se afla in agenda: " + result);

        boolean result1 = a1.containsContact(c4);
        System.out.println("c4 se afla in agenda: " + result1);

        System.out.println(a1);
    }
}