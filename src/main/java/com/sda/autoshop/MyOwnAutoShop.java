package com.sda.autoshop;

import com.sda.autoshop.model.*;

/**
 * Create a super class called Car. The Car class has the following fields:
 * * speed;
 * * regularPrice;
 * * color;
 * The Car class should have the next method: getSalePrice()
 * <p>
 * Create a subclass of Car class and name it as Truck. The Truck class has the following field: weight and method:
 * getSalePrice()   //if weight>2000; 10%discount.
 * <p>
 * Create a subclass of Car class and name it as Ford. The Ford class has the following fields: year, manufacturerDiscount;
 * Method: getSalePrice(); // From the sale price computed from Car class, subtract the manufacturer Discount.
 * <p>
 * Create a subclass of Car class and name it as Sedan. The Sedan class has the following field: length;
 * Method: getSalePrice(); // if length>20feet, 5%discount, otherwise, 10%discount
 * <p>
 * Create MyOwnAutoShop class which contains the main() method. Perform the following within the main() method.
 * <p>
 * Create an instance of Sedan class and initialize all the fields with appropriate values.
 * Use super(...) method in the constructor for initializing the fields of the superclass.
 * <p>
 * Create two instances of the Ford class and initialize all the fields with appropriate values.
 * Use super(...) method in the constructor for initializing the fields of the super class
 * <p>
 * Create an instance of Car class and initialize all the fields with appropriate values.
 * Display the sale prices of all instance.
 */
public class MyOwnAutoShop {
    public static void main(String[] args) {
        Sedan sedan = new Sedan(50, 7000, Color.RED, 15);
        Ford ford = new Ford(60, 5000, Color.BLACK, 17, 0.10);
//        Car car = new Car(80, 1000, Color.YELLOW);  // clasa abstracta nu poate fi instantiata
        Car car = new Car(80, 1000, Color.YELLOW) { // clasa anonima
            @Override
            public double getSalePrice() {
                return this.getRegularPrice();
            }
        };

        Truck truck = new Truck(160, 10000.0, Color.GREEN, 5000);
        System.out.println("The sale price of Sedan is:" + sedan.getSalePrice());
        System.out.println("The sale price of Ford is:" + ford.getSalePrice());
//        System.out.println("The sale price of Car is:" + car.getSalePrice());
        System.out.println(sedan);

        displayMessageBasedOnInstance(sedan);
        displayMessageBasedOnInstance(ford);
        displayMessageBasedOnInstance(truck);
//        displayMessageBasedOnInstance(car);

        Mercedes mercedes = new Mercedes(260, 20_000.0, Color.BLACK, null);
        System.out.println("THe sale price for mercedes is: " + mercedes.getSalePrice());

        Car.displayCar();
    }

    private static void displayMessageBasedOnInstance(Car car) { // polimorfism => realizat prin mostenire
//        if (car.getClass() == Sedan.class) {
//        }
        // the same with
        if (car instanceof Sedan) {
            System.out.println("Sedan...");
        } else if (car instanceof Ford) {
            System.out.println("Ford...");
        } else if (car instanceof Truck) {
            System.out.println("Truck");
        } else {
            System.out.println("Other Car...");
        }

        System.out.println("Checked!");
    }

}

