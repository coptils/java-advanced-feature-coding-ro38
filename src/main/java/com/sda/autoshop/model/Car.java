package com.sda.autoshop.model;

public abstract class Car {
    private  int speed;
    private double regularPrice;
    private Color color;

    public Car(int speed, double regularPrice,Color color){
        this.speed = speed;
        this.regularPrice=regularPrice;
        this.color=color;
    }

    protected double getRegularPrice() {  // doar copiii si clasele din acelasi pachet
        return regularPrice;
    }

    public abstract double getSalePrice();

    public static void displayCar() {
        System.out.println("My Car from static method has bla bla bla " );
    }

    @Override
    public String toString() {
        return
                "speed=" + speed +
                        ", regularPrice=" + regularPrice +
                        ", color=" + color +
                        ",";
    }

}

