package com.sda.autoshop.model;

public enum Color {
    RED, BLACK, YELLOW, GREEN;
}
