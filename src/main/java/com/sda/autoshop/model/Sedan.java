package com.sda.autoshop.model;

import com.sda.autoshop.model.Car;
import com.sda.autoshop.model.Color;

public class Sedan extends Car {
    private int length;

    public Sedan(int speed, double regularPrice, Color color, int length){
        super(speed, regularPrice, color);
        this.length=length;

    }

    @Override
    public double getSalePrice() {
        double salePrice = super.getRegularPrice();
        if(length >20){
            return salePrice - salePrice * 0.05;
        }
        return  salePrice - salePrice * 0.10;
    }

    @Override
    public String toString() {
        return "Sedan{" +
                super.toString()+
                "length=" + length +
                '}';
    }
}
