package com.sda.autoshop.model;

public class Mercedes extends Car {
    private String type;

    public Mercedes(int speed, double regularPrice, Color color, String type) {
        super(speed, regularPrice, color);
        this.type = type;
    }

    @Override
    public double getSalePrice() {
//        return type.equals("gas") ?   // this could throw an NPE exception
//        return Objects.equals("gas", type) ?
        return "gas".equals(type) ?
                super.getRegularPrice() + super.getRegularPrice() * 0.10
                :
                super.getRegularPrice() + super.getRegularPrice() * 0.05;
    }
}
