package com.sda.autoshop.model;

import com.sda.autoshop.model.Car;
import com.sda.autoshop.model.Color;

public class Truck extends Car {
    private int weight;

    public Truck(int speed, double regularPrice, Color color, int weight) {
        super(speed, regularPrice, color);
        this.weight = weight;
    }

    @Override
    public double getSalePrice(){
        double salePrice= super.getRegularPrice();
        if(weight>2000){
            return salePrice -salePrice *0.10;
        }
        return salePrice;
    }
}
