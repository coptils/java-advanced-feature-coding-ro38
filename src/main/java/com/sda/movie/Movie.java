package com.sda.movie;

public class Movie {

    private String title;
    private String genre;
    private String director;
    private int releaseYear;

    public Movie(String title, String genre, String director, int releaseYear) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.releaseYear = releaseYear;
    }

    @Override
    public String toString() {
        return "\n" + title + "," + director + "," + genre +","+ releaseYear;
    }
}
