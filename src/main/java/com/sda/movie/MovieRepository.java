package com.sda.movie;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class MovieRepository {

    private static final Path PATH = Paths.get("src/main/resources/movies.txt");

    public void addMovie(Movie movie) throws IOException {
        Files.writeString(PATH, movie.toString(), StandardOpenOption.APPEND);
    }


    public List<Movie> getMovies() throws IOException{
       Files.readAllLines(PATH);
       List<String> fileLines = Files.readAllLines(PATH);

        List<Movie> movies = new ArrayList<>();
        for (String string:fileLines) {
            String[] myArray = string.split(",");

            Movie movie = new Movie(myArray[0], myArray[2], myArray[1], Integer.parseInt(myArray[3]));
            movies.add(movie);

        }
       return movies;
    }


}
