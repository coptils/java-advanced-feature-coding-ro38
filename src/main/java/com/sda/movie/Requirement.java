package com.sda.movie;

import com.github.javafaker.Faker;
import com.sun.jdi.PathSearchingVirtualMachine;

import java.io.IOException;
import java.util.List;

/**
 * Create a program which will provide following features based on the Movie class objects:
 * adding objects,
 * returning object lists.
 * The Movie class should contain fields: title, genre, director, releaseYear.
 * <p>
 * Adding objects should be written to a file.
 * Displaying object list should read the text file to convert individual lines to Movie objects.
 */
public class Requirement {
    public static void main(String[] args) throws IOException {
        Faker faker = new Faker();
        Movie m1 =new Movie(faker.book().title(), faker.book().genre(), faker.book().author(), faker.number().numberBetween(1700, 2022));

        MovieRepository movieRepository = new MovieRepository();
        movieRepository.addMovie(m1);


        List<Movie> movies =  movieRepository.getMovies();
        System.out.println(movies);

    }

}
