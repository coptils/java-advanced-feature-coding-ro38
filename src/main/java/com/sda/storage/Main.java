package com.sda.storage;

/**
 * Create a Storage class that will have a private Map field, a public constructor, and methods:
 * The Storage class should allow you to store multiple values under one key.
 * *** addToStorage(String key, String value) → adding elements to the storage
 * *** printValues(String key) → displaying all elements under a given key
 * *** findValues(String value) → displaying all keys that have a given value
 */

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();

        storage.addToStorage("1","ro");
        storage.addToStorage("1","uk");
        storage.addToStorage("2","fr");
        storage.addToStorage("2","uk");
        storage.addToStorage("3","uk");
        storage.addToStorage("3","fr");

        storage.displayMap();
        storage.findKeys("ro");
    }
}
