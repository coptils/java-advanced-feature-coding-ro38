package com.sda.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create a Storage class that will have a private Map field, a public constructor, and methods:
 * The Storage class should allow you to store multiple values under one key.
 * *** addToStorage(String key, String value) → adding elements to the storage
 * *** printValues(String key) → displaying all elements under a given key
 * *** findValues(String value) → displaying all keys that have a given value
 */

public class Storage {
    private Map<String, List<String>> storage;

    public Storage() {
        this.storage = new HashMap<String, List<String>>();
    }

    public void addToStorage(String key, String value) {
        if (storage.containsKey(key)) {
            List<String> existingValues = storage.get(key);
            existingValues.add(value);
            storage.put(key, existingValues);
        } else {
            List<String> lista = new ArrayList<>();
            lista.add(value);
            storage.put(key, lista);
        }
    }

    public void displayMap() {
        storage.forEach((key, values) -> System.out.println(key + "->" + values));
    }

    public void findKeys(String value) {
        storage.forEach((key, values) -> {
            if (values.contains(value)) {
                System.out.println(key);
            }
        });
    }
}


